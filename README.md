# Mock Payment Gateway (PHP)

##Description
A mock payment gateway specific for this technical challenge. Functions included:
 - Receive payment and respond with payment reference code or error on payment succeed and failed respectively
 - Checking credit card type

## Deployment
 - Deploy by Docker automatically at build time.
 
## Third-party libraries
 - [Hashids](https://github.com/ivanakimov/hashids.php)
 - [PHP Credit Card Validator](https://github.com/inacho/php-credit-card-validator)