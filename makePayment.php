<?php
session_start();
date_default_timezone_set("Asia/Hong_Kong");
$now = new DateTime();
require_once("vendor/autoload.php");

use Hashids\Hashids;
use Inacho\CreditCard;

try {
    $ENV_PREFIX = strtoupper(basename(__DIR__));
    $REFERENCE_CODE_PREFIX = getenv($ENV_PREFIX . '_REFERENCE_CODE_PREFIX');
    $REFERENCE_CODE_LENGTH = getenv($ENV_PREFIX . '_REFERENCE_CODE_LENGTH');
    $REJECTED_CARD_TYPES = getenv($ENV_PREFIX . '_REJECTED_CARD_TYPES');
    $HASHID_SALT = getenv($ENV_PREFIX . '_HASHID_SALT');

    $rejectedCardTypes = explode(",", strtolower($REJECTED_CARD_TYPES));
    $hashIds = new Hashids($HASHID_SALT, $REFERENCE_CODE_LENGTH);

    if (isset($_POST["number"]) && isset($_POST["code"]) && isset($_POST["year"]) && isset($_POST["month"])
        && !(empty($_POST["number"]) || empty($_POST["code"]) || empty($_POST["year"]) || empty($_POST["month"]))) {
        $number = $_POST["number"];
        $code = $_POST["code"];
        $year = $_POST["year"];
        $month = $_POST["month"];

        $card = CreditCard::validCreditCard($number);
        //error_log("[DEBUG] $number|$code|$year|$month|" . $card["type"]);
        if ($card["valid"] === false) {
            throw new Exception("Invalid card number.", 1002);
        }
        if (in_array($card["type"], $rejectedCardTypes) === true) {
            throw new Exception("Card type not accepted.", 1003);
        }
        if (CreditCard::validCvc($code, $card["type"]) === false) {
            throw new Exception("Invalid security code.", 1004);
        }

        $referenceData = str_split($number, 4);
        $referenceData[] = $code;
        $referenceData[] = $year;
        $referenceData[] = $month;
        $referenceData[] = $now->getTimestamp();
        $reference = EncodeData($hashIds, $referenceData);
        if ($reference === false) {
            throw new Exception("Failed to generate payment code.");
        }
        $reference = substr($reference, 0, $REFERENCE_CODE_LENGTH);
        $reference = $REFERENCE_CODE_PREFIX . "-" . strtoupper($reference);

        RespondData(array("reference" => $reference));
    } else {
        throw new Exception("Invalid parameter(s).", 1001);
    }


} catch (Exception $e) {
    RespondError($e->getMessage());
}

exit();
#############################################
function RespondData($data)
{
    header("Content-Type: application/json; charset=UTF-8");
    _Respond(json_encode(array("data" => $data)), 200);
}

function _Respond($data, $httpStatus, $noCache = false)
{
    if (function_exists("http_response_code")) {
        http_response_code($httpStatus);
    } else {
        header("HTTP/1.1 $httpStatus", true, $httpStatus);
    }

    if ($noCache === true) {
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
    }
    echo($data);
}

function RespondError($message, $code = 0)
{
    $error = array();
    $error["code"] = $code;
    $error["message"] = $message;
    error_log("[" . strtoupper(basename(__DIR__)) . " Exception] " . $message);
    header("Content-Type: application/json; charset=UTF-8");
    _Respond(json_encode(array("error" => $error)), 400);
}

function EncodeData($hashIds, array $data, $hashLimit = 1000000000)
{
    try {
        $preHash = array();
        foreach ($data as $k => $v) {
            $modified = 0;
            if ($v > $hashLimit) {
                $preHash[] = $v - $hashLimit;
                $modified = 1;
            } else {
                $preHash[] = $v;
            }
            $preHash[] = $modified;
        }
        return $hashIds->encode($preHash);
    } catch (Exception $e) {
        error_log($e->getMessage());
        return false;
    }
}
